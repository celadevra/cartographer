require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  include Sorcery::TestHelpers::Rails::Controller
  setup do
    @user = users(:one)
    login_user(@user, route = login_url)
  end

  test "should show user" do
    get :show, id: @user
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create new user" do
    assert_difference('User.count') do
      post :create, user: { name: "test user", email: "test@example.com", public_email: "test2@example.com", location: "LA", password: "secret", password_confirmation: "secret" }
    end
    assert_redirected_to user_path(assigns(:user))
  end

  test "should get edit" do
    get :edit, id: @user.id
    assert_response :success
  end

  test "should update user" do
    patch :update, id: @user.id, user: { name: @user.name, email: @user.email, public_email: @user.public_email, location: @user.location, organization: @user.organization }
    assert_redirected_to user_path(assigns(:user))
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete :destroy, id: @user.id
    end

    assert_redirected_to root_path
  end
  # test "the truth" do
  #   assert true
  # end
end
