require 'test_helper'
require 'sorcery'

class UserSessionTest < ActionDispatch::IntegrationTest
  include Sorcery::TestHelpers::Rails::Controller
  include Sorcery::TestHelpers::Rails::Integration
  setup do
    @user = users(:one)
    @user2 = users(:two)
  end

  test "should log in user" do
    get root_url
    assert_select "a", "Login"
    get login_path
    post '/user_sessions', email: "t1@example.com", password: "secret"
    assert_redirected_to user_path(@user)
  end

  test "should log out user" do
    get login_path
    post '/user_sessions', email: "t1@example.com", password: "secret"
    get user_path(@user)
    assert_select "a", "Logout"
    post '/logout'
    assert_redirected_to root_path
  end
end
