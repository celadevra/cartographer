# 基于 git 的地图制作工具

Git 可以用来保存地图脚本，并进行版本控制。类似地，版本控制工具也可以用来管理数据的版本变化。就是不太清楚对于 PostGIS 这样的版本变化如何跟踪和记录，还要支持回滚和回放。

实现：每个用户要有多个 Git repo，支持 clone 到线下，以及 fork、pull request 等操作。每个 repo 是一个生成地图的脚本，还包括数据的 submodule。这样用户可以选择使用最新的数据，也可以选择将数据版本固定在某一版。

数据的存放：根据脚本中的信息，读取数据源，验证其可用性，然后导入 PostGIS 中。用户要使用时，再导出成 geoJSON 或类似格式，存放在云端，便于 leaflet 等调用。

数据的显示：将云端缓存或刚刚生成的数据以 geoJSON 格式传到 leaflet，然后按照用户指定的格式显示出来。

# 后台设计

## 用户 model

每个账户对应系统中一个 user，user 对象在数据库中包括以下字段：

* name - string
* email - string
* location - the\_geom
* organization - string
* public email - string
* bitbucket\_access\_token - string
* bitbucket\_access\_secret - string
* github\_access\_token
* gitlab\_access\_token
* coding\_access\_token

用户 has\_many repos。
