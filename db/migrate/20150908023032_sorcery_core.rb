class SorceryCore < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name,             :null => false
      t.string :email,            :null => false
      t.string :public_email
      t.string :location
      t.string :organization
      t.string :crypted_password
      t.string :salt

      t.timestamps
    end

    add_index :users, :email, unique: true
  end
end
